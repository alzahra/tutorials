extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600
export (int) var running_speed = 900

const UP = Vector2(0,-1)

export (Vector2) var velocity = Vector2()

onready var sprite = $Sprite
export(Texture) var sprite_jump
export(Texture) var sprite_normal
export(Texture) var sprite_run
export(Texture) var sprite_walk

var has_jumped = false

func get_input():
	velocity.x = 0
	if Input.is_action_just_pressed('up') and has_jumped:
		velocity.y = jump_speed
		has_jumped = false
		sprite.texture = sprite_jump
	elif is_on_floor() and Input.is_action_just_pressed('up'):
		velocity.y = jump_speed
		sprite.texture = sprite_jump
		has_jumped = true
	elif Input.is_key_pressed(KEY_SHIFT) and Input.is_action_pressed('right'):
		velocity.x += running_speed
		sprite.set_flip_h(false)
		sprite.texture = sprite_run
	elif Input.is_key_pressed(KEY_SHIFT) and Input.is_action_pressed('left'):
		velocity.x -= running_speed
		sprite.set_flip_h(true)
		sprite.texture = sprite_run
	elif Input.is_action_pressed('right'):
		velocity.x += speed
		sprite.set_flip_h(false)
		sprite.texture = sprite_walk
	elif Input.is_action_pressed('left'):
		velocity.x -= speed
		sprite.set_flip_h(true)
		sprite.texture = sprite_walk
	elif is_on_floor():
		sprite.texture = sprite_normal
		
func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)
